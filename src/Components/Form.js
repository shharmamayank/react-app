import React, { useState } from 'react'
import Axios from 'axios';
import './Form.css'

function Form() {
  const url = "";
  const [data, setData] = useState({
    name: "",
    date: "",
    count: ""

  })
  function submit(a){
    a.preventDefault();
    Axios.post(url,{
      name:data.name,
      date:data.date,
      count:data.count
    }).then(res=>{
      console.log((res.data));
    })

  }
  function handleChange(a) {
    const newdata = { ...data }
    newdata[a.target.id] = a.target.value
    setData(newdata)
    console.log(newdata);

  }
  return (
    <div className='form-div'>
      <form onSubmit={(a)=>submit(a)}>
        <input onChange={(a) => handleChange(a)} id="name" value={data.name} placeholder='Name' type='text'></input>
        <input onChange={(a) => handleChange(a)} id="date" value={data.date} placeholder='date' type='date'></input>
        <input onChange={(a) => handleChange(a)} id="count" value={data.count} placeholder='count' type='number'></input>
        <button>Submit</button>
      </form>

    </div>
  )
}

export default Form
